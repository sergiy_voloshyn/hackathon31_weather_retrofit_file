package com.sourceit.simpleweather;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sourceit.simpleweather.model.Forecast;
import com.sourceit.simpleweather.model.Weather;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.sourceit.simpleweather.adapter.ForecastAdapter.TIME_FORECAST;


public class OneDayFragment extends Fragment {
    @BindView(R.id.description_one_day)
    TextView descriptionOneDay;

    @BindView(R.id.temperature_one_day)
    TextView temperatureOneDay;

    @BindView(R.id.temperature_asfeel_one_day)
    TextView temperatureAsfeelOneDay;

    @BindView(R.id.pressure_one_day)
    TextView pressureOneDay;

    @BindView(R.id.time_rise_sun_one_day)
    TextView timeDawnSunOneDay;

    @BindView(R.id.time_sunset_sun_one_day)
    TextView timeSunsetSunOneDay;

    @BindView(R.id.time_rise_moon_one_day)
    TextView timeDawnMoonOneDay;

    @BindView(R.id.time_sunset_moon_one_day)
    TextView timeSunsetMoonOneDay;

    @BindView(R.id.wind_speed_one_day)
    TextView windSpeedOneDay;

    @BindView(R.id.icon_weather_one_day)
    ImageView iconWeatherOneDay;


    Weather oneDayForecast = null;


    private OneInteractionListener mListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_one_day, container, false);
        ButterKnife.bind(this, view);
        if (oneDayForecast != null) {
            // at 12-00

            descriptionOneDay.setText(oneDayForecast.hourly.get(TIME_FORECAST).getWeatherDesc().get(0).getValue());
            temperatureOneDay.setText(oneDayForecast.hourly.get(TIME_FORECAST).getTempC());
            temperatureAsfeelOneDay.setText(oneDayForecast.hourly.get(TIME_FORECAST).getFeelsLikeC());
            pressureOneDay.setText(oneDayForecast.hourly.get(TIME_FORECAST).getPressure());
            timeDawnSunOneDay.setText(oneDayForecast.getAstronomy().get(0).getSunrise());
            timeSunsetSunOneDay.setText(oneDayForecast.getAstronomy().get(0).getSunset());
            timeDawnMoonOneDay.setText(oneDayForecast.getAstronomy().get(0).getMoonrise());
            timeSunsetMoonOneDay.setText(oneDayForecast.getAstronomy().get(0).getMoonset());
            int windSpeed = Integer.valueOf(oneDayForecast.hourly.get(TIME_FORECAST).getWindspeedKmph());
            windSpeedOneDay.setText(String.valueOf(windSpeed * 1000 / (60 * 60)));
            String img = oneDayForecast.hourly.get(TIME_FORECAST).getWeatherIconUrl().get(0).getValue();
            Picasso.with(getContext()).load(img).into(iconWeatherOneDay);


        }

        return view;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onOneInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OneInteractionListener) {
            mListener = (OneInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OneInteractionListener {
        void onOneInteraction(Uri uri);
    }


    public void setData(Weather data) {
        if (data != null) {
            this.oneDayForecast = data;
        }
    }

}

/*
Приложение должно уметь:
Показывать краткий прогноз погоды на ближайшие 5 дней для Харькова. (Сюда должно быть включено:
общее описание (облачно, солнечно, возможен дождь), температура воздуха, температура
“как чувствуется”, атмосферное давление и иконку текущей погоды).
Показывать подробный прогноз погоды для каждого дня. (Сюда можно включить информацию
из краткого прогноза + время рассвета/заката солнца, восхода/захода луны,
вероятность дождя/снега/тумана и пр. (4 на выбор из возможных) и скорость ветра).
Обновлять данные только раз в сутки (если пользователь заходит в приложение),
в остальное время - использовать кэш (сохранённые данные во время предыдущей загрузки).


 */
