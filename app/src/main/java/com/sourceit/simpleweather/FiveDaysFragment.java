package com.sourceit.simpleweather;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sourceit.simpleweather.adapter.ForecastAdapter;
import com.sourceit.simpleweather.model.Forecast;
import com.sourceit.simpleweather.model.Weather;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FiveDaysFragment extends Fragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    ForecastAdapter forecastAdapter;
    LinearLayoutManager layoutManager;

    public List<Weather> forecastList = null;

    private FiveInteractionListener mListener;


    public void setListData(List<Weather> dataList) {
        if (dataList != null) {
            this.forecastList = dataList;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_five_days, container, false);
        ButterKnife.bind(this, view);

        if (forecastList != null) {
        forecastAdapter = new ForecastAdapter(forecastList, getContext());
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(forecastAdapter);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OneDayFragment.OneInteractionListener) {
            mListener = (FiveInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface FiveInteractionListener {

        void onFiveInteraction(Uri uri);

        public List<Weather> getDataList();
    }

}