package com.sourceit.simpleweather.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sourceit.simpleweather.R;
import com.sourceit.simpleweather.model.Data;
import com.sourceit.simpleweather.model.Forecast;
import com.sourceit.simpleweather.model.Weather;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    public List<Weather> list;
    private Context context;
    public static int TIME_FORECAST = 4;

    public ForecastAdapter(List<Weather> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_forecast, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_description)
        TextView description;
        @BindView(R.id.tv_temperature)
        TextView temperature;
        @BindView(R.id.tv_temperature_as_fell)
        TextView temperatureAsFeel;
        @BindView(R.id.tv_date)
        TextView dateForecast;
        @BindView(R.id.image_view)
        ImageView image;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Weather forecast) {
            description.setText(forecast.hourly.get(TIME_FORECAST).getWeatherDesc().get(0).getValue());
            temperature.setText(forecast.hourly.get(TIME_FORECAST).getTempC());
            temperatureAsFeel.setText(forecast.hourly.get(TIME_FORECAST).getFeelsLikeC());
            dateForecast.setText(forecast.getDate());
            Picasso.with(context).load(forecast.hourly.get(TIME_FORECAST).getWeatherIconUrl().get(0).getValue()).into(image);
        }
    }

}