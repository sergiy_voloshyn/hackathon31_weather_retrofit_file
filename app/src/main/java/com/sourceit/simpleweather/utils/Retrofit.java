package com.sourceit.simpleweather.utils;

import com.sourceit.simpleweather.model.Forecast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

public class Retrofit {

    private static final String ENDPOINT = "http://api.worldweatheronline.com";
    // "?key=e404e042bbbe487497b80733182402
    // &q=kharkiv
    // &format=json
    // &num_of_days=5";

    private static final String keyAPI = "e404e042bbbe487497b80733182402";
    private static final String cityAPI = "kharkiv";
    private static final String formatAPI = "json";
    private static final String numDaysAPI = "15";


    private static ApiInterface apiInterface;

    static {
        initialize();
    }
    //http://api.worldweatheronline.com/premium/v1/weather.ashx?key=760fefb16f8f445b86c82749182402&q=Kharkiv&format=json&num_of_days=5

    interface ApiInterface {

        @GET("/premium/v1/weather.ashx")
        void getWeatherOneDay(@Query("key") String key,
                              @Query("q") String q,
                              @Query("format") String format,
                              @Query("num_of_days") String num,
                              @Query("date") String date,
                              Callback<Forecast> callback);

        @GET("/premium/v1/weather.ashx")
        void getWeatherFiveDay(@Query("key") String key,
                               @Query("q") String q,
                               @Query("format") String format,
                               @Query("num_of_days") String num,
                               Callback<Forecast> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getWeatherOneDay(Callback<Forecast> callback) {
        //"date": "2018-02-25"
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);

        apiInterface.getWeatherOneDay(keyAPI, cityAPI, formatAPI, numDaysAPI, formattedDate, callback);
    }

    public static void getWeatherFiveDays(Callback<Forecast> callback) {
        apiInterface.getWeatherFiveDay(keyAPI, cityAPI, formatAPI, numDaysAPI, callback);
    }
}