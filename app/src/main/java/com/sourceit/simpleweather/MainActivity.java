package com.sourceit.simpleweather;

import android.content.Context;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.sourceit.simpleweather.model.Forecast;
import com.sourceit.simpleweather.model.Weather;
import com.sourceit.simpleweather.utils.Retrofit;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements OneDayFragment.OneInteractionListener,
        FiveDaysFragment.FiveInteractionListener {


    OneDayFragment oneDayFragment;
    FiveDaysFragment fiveDaysFragment;

    public Forecast forecastInfo;
    public List<Weather> fiveDaysForecast;
    public Weather oneDayForecast;


    public final String FILENAME = "info.json";
    boolean isCurrentDate=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        FileInputStream fis = null;
        ObjectInputStream inputStream = null;

        oneDayFragment = new OneDayFragment();
        fiveDaysFragment = new FiveDaysFragment();

        boolean fileFound = false;

        try {
            fis = openFileInput(FILENAME);
            inputStream = new ObjectInputStream(fis);
            forecastInfo = (Forecast) inputStream.readObject();


        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                    fileFound = true;
                }
                if (inputStream != null) inputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (fileFound) {
            //data already set to forecastInfo
            if (forecastInfo != null && forecastInfo.getList().size() > 0) {

                fiveDaysForecast = forecastInfo.getList();
                oneDayForecast = forecastInfo.getListItem(0);

                String fileDate =oneDayForecast.getDate();

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String currentDate = df.format(c);

                if (fileDate.equals(currentDate)){
                    isCurrentDate=true;

                fiveDaysFragment.setListData(fiveDaysForecast);
                oneDayFragment.setData(oneDayForecast);

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.base_container, oneDayFragment)
                        .addToBackStack(null)
                        .commit();
                }
            }
        }
        if (!fileFound || !isCurrentDate) {
            Retrofit.getWeatherFiveDays(new Callback<Forecast>() {
                @Override
                public void success(Forecast forecasts, Response response) {

                    FileOutputStream fos = null;
                    ObjectOutputStream outputStream = null;

                    try {
                        fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                        outputStream = new ObjectOutputStream(fos);
                        outputStream.writeObject(forecasts);

                        forecastInfo = forecasts;
                        fiveDaysForecast = forecasts.getList();
                        oneDayForecast = forecasts.getListItem(0);

                        fiveDaysFragment.setListData(fiveDaysForecast);
                        oneDayFragment.setData(oneDayForecast);

                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.base_container, oneDayFragment)
                                .addToBackStack(null)
                                .commit();


                    } catch (Exception ex) {
                        ex.printStackTrace();
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Alert")
                                .setMessage(ex.toString())
                                .setCancelable(true)
                                .show();
                    } finally {
                        try {
                            if (fos != null) fos.close();
                            if (outputStream != null) outputStream.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Alert")
                            .setMessage(error.toString())
                            .setCancelable(true)
                            .show();
                }
            });
        }

    }

    @OnClick(R.id.button_one_day)
    public void onClickOneDayButton() {
        if (oneDayFragment != null && oneDayForecast != null) {
            oneDayFragment.setData(oneDayForecast);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.base_container, oneDayFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @OnClick(R.id.button_five_day)
    public void onClickFiveDayButton() {
        if (fiveDaysFragment != null && fiveDaysForecast != null) {
            fiveDaysFragment.setListData(fiveDaysForecast);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.base_container, fiveDaysFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }


    @Override
    public void onOneInteraction(Uri uri) {

    }

    @Override
    public void onFiveInteraction(Uri uri) {

    }

    @Override
    public List<Weather> getDataList() {

        if (fiveDaysForecast != null) {
            //  return this.listForecastInfo;
            return null;

        } else {
            return null;
        }
    }
}
/*
Создать приложение “Прогноз погоды”. Для получения прогноза использовать
 API сайта http://www.worldweatheronline.com/api/. В приложении должно быть 2 экрана
 (с кратким прогнозом на 5 дней и подробным прогнозом на один выбранный день).
 Приложение должно уметь:
Показывать краткий прогноз погоды на ближайшие 5 дней для Харькова. (Сюда должно быть включено:
общее описание (облачно, солнечно, возможен дождь), температура воздуха, температура “как чувствуется”,
атмосферное давление и иконку текущей погоды).
Показывать подробный прогноз погоды для каждого дня. (Сюда можно включить информацию из краткого прогноза
+ время рассвета/заката солнца, восхода/захода луны, вероятность дождя/снега/тумана и пр.
(4 на выбор из возможных) и скорость ветра).
Обновлять данные только раз в сутки (если пользователь заходит в приложение), в остальное время -
 использовать кэш (сохранённые данные во время предыдущей загрузки).


Sprint 2.

Дополнить приложение экраном настроек и иконки notification’a о текущей температуре воздуха.
В приложении должно быть:
Экран настроек, в котором пользователь может ввести свой текущий город и выбор показа температуры
(по Фаренгейту или Цельсию).
Возможность ручного обновления данных (возможно в системном меню или по клику на кнопу “Update”).
И notification, в котором будет показан текущий город и текущая температура воздуха.

 */