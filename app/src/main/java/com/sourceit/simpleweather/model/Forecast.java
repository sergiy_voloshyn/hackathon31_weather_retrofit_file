package com.sourceit.simpleweather.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by blago on 2/24/18.
 */

public class Forecast implements Serializable {

//    public String date;
//    public String description;
//    public String temperature;
//    public String temperatureAsFeel;
//    public String pressure;
//    public String iconWeather;
//    public String sunrise;
//    public String sunset;
//    public String moonrise;
//    public String moonset;

    public String getDescription() {
        return getData().getCurrentCondition().get(0).getWeatherDesc().get(0).getValue();
    }

    public String getTemperature() {
        return getData().getCurrentCondition().get(0).getTempC();
    }

    public String getTemperatureAsFeel() {
        return getData().getCurrentCondition().get(0).getFeelsLikeC();
    }

    public String getPressure() {
        return getData().getCurrentCondition().get(0).getPressure();
    }

    public String getIconWeather() {
        return getData().getCurrentCondition().get(0).getWeatherIconUrl().get(0).getValue();
    }

    public String getSunrise() {
        return getData().getWeather().get(0).getAstronomy().get(0).getSunrise();
    }

    public String getSunset() {
        return getData().getWeather().get(0).getAstronomy().get(0).getSunset();
    }

    public String getMoonrise() {
        return getData().getWeather().get(0).getAstronomy().get(0).getMoonrise();
    }

    public String getMoonset() {
        return getData().getWeather().get(0).getAstronomy().get(0).getMoonset();
    }
    public String getWindSpeed() {
         return getData().getCurrentCondition().get(0).getWindspeedKmph();
    }

    public List<Weather> getList() {
        return getData().getWeather();
    }

    public int getListSize() {
        return getData().getWeather().size();
    }
    public Weather getListItem(int index) {
        return getData().getWeather().get(index);
    }

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
