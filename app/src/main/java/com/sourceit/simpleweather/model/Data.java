
package com.sourceit.simpleweather.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable{

    @SerializedName("current_condition")
    @Expose
    private List<CurrentCondition> currentCondition = null;
    @SerializedName("weather")

    @Expose
    public List<Weather> weather = null;


    public List<CurrentCondition> getCurrentCondition() {
        return currentCondition;
    }

    public void setCurrentCondition(List<CurrentCondition> currentCondition) {
        this.currentCondition = currentCondition;
    }

   public List<Weather> getWeather() {        return weather;    }


    public void setWeather(List<Weather> weather) {       this.weather = weather;    }

}
