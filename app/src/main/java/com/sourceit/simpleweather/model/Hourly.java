package com.sourceit.simpleweather.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by user on 27.02.2018.
 */

public class Hourly implements Serializable {

    String time;
    String tempC;

    String windspeedKmph;
    List<WeatherIconUrl> weatherIconUrl;
    List<WeatherDesc> weatherDesc;
    String pressure;
    String FeelsLikeC;

    public String getTime() {
        return time;
    }

    public String getTempC() {
        return tempC;
    }

    public String getWindspeedKmph() {
        return windspeedKmph;
    }

    public List<WeatherIconUrl> getWeatherIconUrl() {
        return weatherIconUrl;
    }

    public List<WeatherDesc> getWeatherDesc() {
        return weatherDesc;
    }

    public String getPressure() {
        return pressure;
    }

    public String getFeelsLikeC() {
        return FeelsLikeC;
    }
}
